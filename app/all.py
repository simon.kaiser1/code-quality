from flask import Blueprint
bp = Blueprint("all", __name__)


@bp.route("/")
def index():
    return "Hello!"
